package com.diplomski.diplomski

import android.app.Application
import com.diplomski.diplomski.data.api.RestApiClient

/**
 * Created by Azra on 29.3.2019.
 */
class BaseApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        RestApiClient.init()
    }

}