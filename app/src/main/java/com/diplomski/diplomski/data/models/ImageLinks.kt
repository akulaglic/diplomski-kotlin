package com.diplomski.diplomski.data.models

import java.io.Serializable

/**
 * Created by Azra on 25.5.2019.
 */
class ImageLinks(val smallThumbnail : String,
                 val thumbnail : String) : Serializable {

}