package com.diplomski.diplomski.data.models

import java.io.Serializable

/**
 * Created by Azra on 30.3.2019.
 */
class VolumeInfo : Serializable{
    var title = ""
    var authors : ArrayList<String> = ArrayList()
    var description = ""
    var pageCount = 0
    var publishedDate = ""
    var id = ""
    var imageLinks : ImageLinks? = null

}