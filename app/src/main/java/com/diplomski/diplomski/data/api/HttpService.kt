package com.diplomski.diplomski.data.api
import com.diplomski.diplomski.data.models.response.BooksResponse
import retrofit2.Call
import retrofit2.Callback

/**
 * Created by Azra on 29.3.2019.
 */
class HttpService {
    companion object {
        fun getBooks(q: String,
                     maxResults: String,
                     orderBy: String,
                     callback: Callback<BooksResponse>) {
            val call : Call<BooksResponse> = RestApiClient.getService().getBooks(q, maxResults, orderBy)
            call.enqueue(callback)
        }
    }
}