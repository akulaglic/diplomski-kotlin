package com.diplomski.diplomski.data.api

import okhttp3.Interceptor
import okhttp3.Response

/**
 * Created by Azra on 29.3.2019.
 */
class HeaderInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain?): Response {
        val request = chain?.request()?.newBuilder()?.
                addHeader("Accept", "*/*")?.
                addHeader("Content-Type", "application/json")?.build()
        return chain?.proceed(request)!!
    }

}