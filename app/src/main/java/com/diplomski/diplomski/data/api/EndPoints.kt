package com.diplomski.diplomski.data.api

import com.diplomski.diplomski.data.models.response.BooksResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Azra on 29.3.2019.
 */
interface EndPoints {

    @GET("books/v1/volumes")
    fun getBooks(@Query("q") q : String,
                 @Query("maxResults") maxResults : String,
                 @Query("orderBy") orderBy : String) : Call<BooksResponse>

}