package com.diplomski.diplomski.data.database

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import com.diplomski.diplomski.data.models.Author
import com.diplomski.diplomski.data.models.Book
import java.net.URL
import java.util.ArrayList

/**
 * Created by Azra on 30.3.2019.
 */
class DataBaseOpenHelper(c: Context) : SQLiteOpenHelper(c, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(sqLiteDatabase: SQLiteDatabase) {
        sqLiteDatabase.execSQL(DATABASE_CREATE_CATEGORY)
        sqLiteDatabase.execSQL(DATABASE_CREATE_BOOK)
        sqLiteDatabase.execSQL(DATABASE_CREATE_AUTHOR)
        sqLiteDatabase.execSQL(DATABASE_CREATE_AUTHORSHIP)
    }

    override fun onUpgrade(sqLiteDatabase: SQLiteDatabase, i: Int, i1: Int) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_CATEGORY)
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_BOOK)
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_AUTHOR)
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_AUTHORSHIP)

        onCreate(sqLiteDatabase)
    }

    fun readCategory(): ArrayList<String> {

        val list = ArrayList<String>()

        val db = this.readableDatabase
        val query = "SELECT * FROM " + DATABASE_TABLE_CATEGORY
        val cursor = db.rawQuery(query, null)

        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    list.add(cursor.getString(cursor.getColumnIndex(CATEGORY_NAME)))
                } while (cursor.moveToNext())

            }
            cursor.close()
        }

        cursor!!.close()

        return list
    }

    fun addCategory(name: String): Long {
        var id: Long = -1

        val db = this.writableDatabase
        val new = ContentValues()
        new.put(CATEGORY_NAME, name)

        val query = "Select * from $DATABASE_TABLE_CATEGORY where $CATEGORY_NAME='$name'"
        val cursor = db.rawQuery(query, null)

        if (cursor.count == 0) {
            id = db.insert(DataBaseOpenHelper.DATABASE_TABLE_CATEGORY, null, new)
        }

        cursor.close()

        return id
    }



    fun addBook(book: Book): Long {
        var id: Long = -1

        var db: SQLiteDatabase

        val new = ContentValues()
        new.put(BOOK_TITLE, book.getBookTitle())
        new.put(BOOK_NUMBER_OF_PAGES, book.getNumberOfPages())
        new.put(BOOK_RELEASE_DATE, book.getReleaseDate())
        new.put(BOOK_DESCRIPTION, book.getDescription())
        new.put(BOOK_IMAGE, book.getImage().toString())
        if (book.imageBitmap != null) {
            new.put(BOOK_IMAGE_BITMAP, String(book.imageBitmap!!, Charsets.ISO_8859_1))
        }
        new.put(BOOK_ID_WEB_SERVIS, book.getId())
        new.put(BOOK_READ, 0)

        db = this.readableDatabase
        val upit = "Select * from " + DATABASE_TABLE_CATEGORY + " where " + CATEGORY_NAME + "='" + book.getCategory() + "'"
        val c = db.rawQuery(upit, null)
        var d : Long = 0
        if (c != null && c.moveToFirst()) {
            d = c.getLong(c.getColumnIndex(CATEGORY_ID))
            c.moveToNext()
        }

        new.put(BOOK_ID_CATEGORY, d)
        c.close()

        val newAuthorship = ContentValues()
        val newAuthor = ContentValues()

        db = this.readableDatabase

        val query = "Select * from " + DATABASE_TABLE_BOOK + " where " + BOOK_ID_WEB_SERVIS + "='" + book.getId() + "'"
        val cursor = db.rawQuery(query, null)

        db = this.writableDatabase
        if (cursor.count == 0) {
            id = db.insert(DataBaseOpenHelper.DATABASE_TABLE_BOOK, null, new)

            for (i in 0 until book.getAuthors()?.size!!) {
                newAuthor.put(AUTHOR_NAME, book.getAuthors()?.get(i)?.mName)
                val queryy = "Select * from " + DATABASE_TABLE_AUTHOR + " where " + AUTHOR_NAME + "='" + book.getAuthors()?.get(i)?.mName + "'"
                val cursor1 = db.rawQuery(queryy, null)

                if (cursor1.count == 0) {
                    val idAutora = db.insert(DataBaseOpenHelper.DATABASE_TABLE_AUTHOR, null, newAuthor)
                    newAuthorship.put(AUTHORSHIP_ID_AUTHOR, idAutora)
                } else {
                    cursor1.moveToNext()
                    val d1 = cursor1.getLong(cursor1.getColumnIndex(AUTHOR_ID))
                    newAuthorship.put(AUTHORSHIP_ID_AUTHOR, d1)
                }

                newAuthorship.put(AUTHORSHIP_ID_BOOK, id)
                db.insert(DataBaseOpenHelper.DATABASE_TABLE_AUTHORSHIP, null, newAuthorship)
                cursor1.close()

            }
        }
        cursor.close()

        return id
    }

    fun getAllBooksFromBase() : ArrayList<String>{
        val books = ArrayList<String>()
        val db = this.readableDatabase
        val query = "Select * from $DATABASE_TABLE_BOOK"
        val cur = db.rawQuery(query, null)


        for (i in 0 until cur.count) {
            cur.moveToNext()
            books.add(cur.getLong(cur.getColumnIndex(BOOK_ID)).toString())
        }
        return books
    }

    fun getBookByCategory(idCategory: Long): ArrayList<Book> {
        val lista = ArrayList<Book>()

        val db = this.readableDatabase
        val query = "Select * from $DATABASE_TABLE_BOOK where $BOOK_ID_CATEGORY=$idCategory"
        val c = db.rawQuery(query, null)

        val query1 = "Select * from $DATABASE_TABLE_CATEGORY where $CATEGORY_ID=$idCategory"
        val cur = db.rawQuery(query1, null)

        cur.moveToNext()
        for (i in 0 until c.count) {
            c.moveToNext()
            val b = Book()
            val s = c.getString(c.getColumnIndex(BOOK_IMAGE))
            try {
                val u = URL(s)
                b.setImage(u)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            try {
                b.imageBitmap = c.getString(c.getColumnIndex(BOOK_IMAGE_BITMAP)).toByteArray(Charsets.ISO_8859_1)
            } catch (e: Exception) {
                e.printStackTrace()
            }


            b.setDescription(c.getString(c.getColumnIndex(BOOK_DESCRIPTION)))
            b.setNumberOfPages(c.getInt(c.getColumnIndex(BOOK_NUMBER_OF_PAGES)))
            b.setReleaseDate(c.getString(c.getColumnIndex(BOOK_RELEASE_DATE)))
            b.setBookTitle(c.getString(c.getColumnIndex(BOOK_TITLE)))
            val id = c.getLong(c.getColumnIndex(BOOK_ID))
            b.setId(id.toString())
            b.setCategory(cur.getString(cur.getColumnIndex(CATEGORY_NAME)))

            val upit2 = "select * from " + DATABASE_TABLE_AUTHOR + " a, " + DATABASE_TABLE_AUTHORSHIP +
                    " a1 where a1.idautora=a._id and  a1." + AUTHORSHIP_ID_BOOK + "=" + id
            val cursor1 = db.rawQuery(upit2, null)
            cursor1.moveToNext()

            if (cursor1.count > 0) {
                val authors = ArrayList<Author>()
                val a = Author()
                val name1 = cursor1.getString(cursor1.getColumnIndex(AUTHOR_NAME))
                a.mName = (name1)
                val d = autorSaViseDijela(cursor1.getLong(cursor1.getColumnIndex(AUTHOR_ID)))
                a.mBooks = d
                authors.add(a)
                b.setAuthors(authors)

                val d1 = autorSaViseDijelaImeAutora(c.getLong(c.getColumnIndex(BOOK_ID)))
                b.setAuthorsName(d1)

                lista.add(b)
                cursor1.close()
            }
        }
        cur.close()

        c.close()

        return lista
    }

    fun getBooksByAuthor(idAutora: Long): ArrayList<Book> {
        val list = ArrayList<Book>()

        val db = this.readableDatabase
        val query = ("select * from " + DATABASE_TABLE_AUTHOR + " a, " + DATABASE_TABLE_AUTHORSHIP + " a1, " +
                DATABASE_TABLE_BOOK + " k where a1.idautora=a._id and k._id=a1.idknjige and a."
                + AUTHOR_ID + "=" + idAutora)
        val c = db.rawQuery(query, null)

        for (i in 0 until c.count) {
            c.moveToNext()
            val  k = Book()
            k.setBookTitle(c.getString(c.getColumnIndex(BOOK_TITLE)))
            val s = c.getString(c.getColumnIndex(BOOK_IMAGE))
            try {
                val u = URL(s)
                k.setImage(u)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            try {
                k.imageBitmap = c.getString(c.getColumnIndex(BOOK_IMAGE_BITMAP)).toByteArray(Charsets.ISO_8859_1) as ByteArray?
            } catch (e: Exception) {
                e.printStackTrace()
            }
            k.setDescription(c.getString(c.getColumnIndex(BOOK_DESCRIPTION)))
            k.setNumberOfPages(c.getInt(c.getColumnIndex(BOOK_NUMBER_OF_PAGES)))
            k.setReleaseDate(c.getString(c.getColumnIndex(BOOK_RELEASE_DATE)))
            val id = c.getLong(c.getColumnIndex(BOOK_ID))
            k.setId(id.toString())

            val description1 = "select * from " + DATABASE_TABLE_BOOK + " k, " + DATABASE_TABLE_CATEGORY +
                    " k1 where k.idkategorije=k1._id and k." + BOOK_ID + "=" + c.getLong(c.getColumnIndex(BOOK_ID))
            val cur = db.rawQuery(description1, null)
            cur.moveToNext()
            k.setCategory(cur.getString(cur.getColumnIndex(CATEGORY_NAME)))
            cur.close()

            val authors = ArrayList<Author>()
            val a = Author()
            val name1 = c.getString(c.getColumnIndex(AUTHOR_NAME))
            a.mName = (name1)
            val d = autorSaViseDijela(c.getLong(c.getColumnIndex(AUTHOR_ID)))
            a.mBooks = (d)
            authors.add(a)
            k.setAuthors(authors)

            val d1 = autorSaViseDijelaImeAutora(c.getLong(c.getColumnIndex(BOOK_ID)))
            k.setAuthorsName(d1)
            list.add(k)
        }

        c.close()

        return list
    }


    fun getCategoryId(category: String): Long {
        val id: Long

        val db = this.readableDatabase
        val s = "select * from $DATABASE_TABLE_CATEGORY where $CATEGORY_NAME='$category'"
        val c = db.rawQuery(s, null)
        c.moveToNext()

        id = c.getLong(c.getColumnIndex(CATEGORY_ID))

        c.close()
        return id
    }

    fun getAuthorId(autor: String): Long? {
        var id: Long? = null

        val db = this.readableDatabase
        val s = "select * from $DATABASE_TABLE_AUTHOR where $AUTHOR_NAME='$autor'"
        val c = db.rawQuery(s, null)
        c.moveToNext()
        if(c.moveToFirst())
        id = c.getLong(c.getColumnIndex(AUTHOR_ID))

        c.close()
        return id
    }

    fun authorsFromBase(): ArrayList<Author> {
        val authors = ArrayList<Author>()

        val db = this.readableDatabase
        val query = "select distinct ime, _id from " + DATABASE_TABLE_AUTHOR
        val c = db.rawQuery(query, null)

        for (i in 0 until c.count) {
            c.moveToNext()
            val a = Author()
            a.mName = (c.getString(c.getColumnIndex(AUTHOR_NAME)))
            var books: ArrayList<String>
            val gg = c.getLong(c.getColumnIndex(AUTHOR_ID))
            books = autorSaViseDijela(gg)

            a.mNumberOfBooks = (books.size)
            authors.add(a)

        }
        c.close()

        return authors
    }

    fun autorSaViseDijela(id: Long): ArrayList<String> {
        val l = ArrayList<String>()
        val db = this.readableDatabase
        val query = ("select * from " + DATABASE_TABLE_AUTHORSHIP + " a, " + DATABASE_TABLE_BOOK
                + " k where a.idknjige=k._id and " + AUTHORSHIP_ID_AUTHOR+ "=" + id)
        val c = db.rawQuery(query, null)

        for (i in 0 until c.count) {
            c.moveToNext()
            l.add(c.getString(c.getColumnIndex(BOOK_TITLE)))
        }

        c.close()
        return l
    }


    fun autorSaViseDijelaImeAutora(id: Long): String {
        var l = ""
        val db = this.readableDatabase
        val query = ("select * from " + DATABASE_TABLE_AUTHORSHIP + " a, " + DATABASE_TABLE_BOOK
                + " k, " + DATABASE_TABLE_AUTHOR + " a1" + " where a.idknjige=k._id and a.idautora=a1._id and "
                + AUTHORSHIP_ID_BOOK + "=" + id)
        val c = db.rawQuery(query, null)

        for (i in 0 until c.count - 1) {
            c.moveToNext()
            l += c.getString(c.getColumnIndex(AUTHOR_NAME)) + ", "
        }
        c.moveToNext()
        l += c.getString(c.getColumnIndex(AUTHOR_NAME))

        c.close()
        return l
    }


    companion object {
        val DATABASE_NAME = "Baza.db"
        val DATABASE_TABLE_CATEGORY = "Kategorija"
        val DATABASE_TABLE_BOOK = "Knjiga"
        val DATABASE_TABLE_AUTHOR = "Autor"
        val DATABASE_TABLE_AUTHORSHIP = "Autorstvo"
        val DATABASE_VERSION = 1
        val CATEGORY_ID = "_id"
        val CATEGORY_NAME = "naziv"
        val BOOK_ID = "_id"
        val BOOK_TITLE = "naziv"
        val BOOK_DESCRIPTION = "opis"
        val BOOK_RELEASE_DATE = "datumObjavljivanja"
        val BOOK_NUMBER_OF_PAGES = "brojStranica"
        val BOOK_ID_WEB_SERVIS = "idWebServis"
        val BOOK_IMAGE = "slika"
        val BOOK_READ = "pregledana"
        val BOOK_ID_CATEGORY = "idkategorije"
        val AUTHOR_ID = "_id"
        val AUTHOR_NAME = "ime"
        val AUTHORSHIP_ID = "_id"
        val AUTHORSHIP_ID_AUTHOR = "idautora"
        val AUTHORSHIP_ID_BOOK = "idknjige"
        val BOOK_IMAGE_BITMAP = "bitmap"

        private val DATABASE_CREATE_CATEGORY = ("create table " + DATABASE_TABLE_CATEGORY
                + " (" + CATEGORY_ID + " integer primary key autoincrement, " +
                CATEGORY_NAME + " text not null);")

        private val DATABASE_CREATE_BOOK = ("create table " + DATABASE_TABLE_BOOK
                + " (" + BOOK_ID + " integer primary key autoincrement, " +
                BOOK_TITLE + " text not null, " + BOOK_DESCRIPTION + " text not null, " +
                BOOK_RELEASE_DATE + " text not null, " + BOOK_NUMBER_OF_PAGES + " integer, "
                + BOOK_ID_WEB_SERVIS + " text not null, " + BOOK_IMAGE + " text not null, "
                + BOOK_READ + " integer, " + BOOK_ID_CATEGORY + " integer, " + BOOK_IMAGE_BITMAP + " text);")

        private val DATABASE_CREATE_AUTHOR = ("create table " + DATABASE_TABLE_AUTHOR
                + " (" + AUTHOR_ID + " integer primary key autoincrement, " + AUTHOR_NAME + " text not null);")


        private val DATABASE_CREATE_AUTHORSHIP = ("create table " + DATABASE_TABLE_AUTHORSHIP
                + " (" + AUTHORSHIP_ID + " integer primary key autoincrement, " + AUTHORSHIP_ID_AUTHOR + " integer, "
                + AUTHORSHIP_ID_BOOK + " integer);")

    }

}
