package com.diplomski.diplomski.data.models

import java.io.Serializable

/**
 * Created by Azra on 28.3.2019.
 */
class Author (var mName : String = "",
              var mBooks : ArrayList<String> = ArrayList<String>(),
              var mNumberOfBooks : Int = 0) : Serializable