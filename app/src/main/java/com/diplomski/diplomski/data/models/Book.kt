package com.diplomski.diplomski.data.models

import java.io.Serializable
import java.net.URL

/**
 * Created by Azra on 28.3.2019.
 */
class Book : Serializable {
    private var id : String = ""
    private var category : String = ""
    private var authors = ArrayList<Author>()
    private var authorsName : String = ""
    private var image : URL? = null
    var imageBitmap : ByteArray? = null

    val volumeInfo = VolumeInfo()

    fun getAuthorName() : String{
        return authorsName
    }

    fun getBookTitle() : String {
        return volumeInfo.title
    }

    fun getNumberOfPages() : Int {
        return volumeInfo.pageCount
    }

    fun getReleaseDate() : String {
        return volumeInfo.publishedDate
    }

    fun getDescription() : String {
        return volumeInfo.description
    }

    fun addAuthor(name : String) {
        authors.add(Author(name))
    }

    fun getImage() : String? {
        return volumeInfo.imageLinks?.thumbnail
    }

    fun getImageURL() : URL? {
        return image
    }

    fun getId() : String {
        return volumeInfo.id
    }
    fun getIdFirst() : String {
        return id
    }
    fun getCategory() : String {
        return category
    }

    fun getAuthors() : ArrayList<Author>? {
        return authors
    }

    fun getAuthorNames() : ArrayList<String>? {
        return volumeInfo.authors
    }

    fun setDescription(description : String) {
        volumeInfo.description = description
    }
    fun setNumberOfPages (pages : Int) {
        volumeInfo.pageCount = pages
    }

    fun setReleaseDate (date : String ) {
        volumeInfo.publishedDate = date
    }

    fun setBookTitle (title : String) {
        volumeInfo.title = title
    }

    fun setId (id : String) {
        volumeInfo.id = id
        this.id = id
    }


    fun setCategory (category : String) {
        this.category = category
    }

    fun setAuthors (authors : ArrayList<Author>) {
        this.authors = authors
    }

    fun setAuthorsName (authorName : String) {
        this.authorsName = authorName
    }

    fun setImage (image : URL) {
        this.image = image
    }


}