package com.diplomski.diplomski.data.models.response

import com.diplomski.diplomski.data.models.Book

/**
 * Created by Azra on 30.3.2019.
 */
class BooksResponse {
    val items : ArrayList<Book> = ArrayList()
}