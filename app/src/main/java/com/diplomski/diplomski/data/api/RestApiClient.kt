package com.diplomski.diplomski.data.api
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import java.util.concurrent.TimeUnit


/**
 * Created by Azra on 29.3.2019.
 */
class RestApiClient {
    companion object {
        var mRetrofitClient : Retrofit? = null
        var mService : EndPoints? = null

        fun getRetrofitClient(baseUrl : String) : Retrofit? {
            if (mRetrofitClient == null) {
                val okHttpClient = OkHttpClient().newBuilder()
                val builder = GsonBuilder()
                val logger = HttpLoggingInterceptor()
                okHttpClient.interceptors().add(HeaderInterceptor())
                okHttpClient.interceptors().add(logger)
                mRetrofitClient = Retrofit.Builder().baseUrl(baseUrl).
                        client(okHttpClient.build()).
                        addConverterFactory(GsonConverterFactory.create(builder.create())).build()
            }
            return mRetrofitClient
        }

        fun getService() : EndPoints {
            if (mService == null) {
                throw IllegalStateException()
            }
            return mService!!
        }

        fun init() {
            mService = getRetrofitClient("https://www.googleapis.com")?.create(EndPoints::class.java)
        }
    }
}