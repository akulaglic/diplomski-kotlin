package com.diplomski.diplomski.ui.adapters

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.diplomski.diplomski.R
import com.diplomski.diplomski.data.models.Book

/**
 * Created by Azra on 30.3.2019.
 */
class OnlineBooksAdapter(val books : ArrayList<Book>?) : BaseAdapter() {

    /**
     * Called as constructor method. Used to create new authors in book.
     */
    init {
        if (books != null) {
            for (book : Book in books) {
                for (name : String in book.getAuthorNames()!!) {
                    book.addAuthor(name)
                }
            }
        }
    }

    @SuppressLint("ViewHolder")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val book = books?.get(p0)
        val textView = LayoutInflater.from(p2?.context).inflate(
                R.layout.simple_list_item, null) as TextView
        textView.text = book?.getBookTitle()
        return textView
    }

    override fun getItem(p0: Int): Any {
        return books?.get(p0)!!
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return books?.size!!
    }

}