package com.diplomski.diplomski.ui.activities.base

import android.os.Bundle
import android.os.PersistableBundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.diplomski.diplomski.R
import com.facebook.stetho.Stetho

/**
 * Created by Azra on 28.3.2019.
 */
abstract class ActivityBase : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        Stetho.initializeWithDefaults(this)
        setContentView(getLayoutId())
        setupActivity()
    }

    /**
     * Override method to return layout resource id that you would usually put in [setContentView]
     * method.
     */
    abstract fun getLayoutId() : Int

    /**
     * Method contains logic after calling [setContentView] in [onCreate] method.
     */
    abstract fun setupActivity()
}