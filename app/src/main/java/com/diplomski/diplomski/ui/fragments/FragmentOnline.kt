package com.diplomski.diplomski.ui.fragments
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import com.diplomski.diplomski.R
import com.diplomski.diplomski.data.api.HttpService
import com.diplomski.diplomski.data.models.Book
import com.diplomski.diplomski.data.models.response.BooksResponse
import com.diplomski.diplomski.ui.adapters.OnlineBooksAdapter
import com.diplomski.diplomski.ui.fragments.base.FragmentBase
import kotlinx.android.synthetic.main.fragment_online.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


/**
 * Created by Azra on 28.3.2019.
 */
class FragmentOnline : FragmentBase() {

    var category : String = ""

    override fun setupFragment(view: View?) {
        btnBack.setOnClickListener(onBack)
        btnOnlineSearch.setOnClickListener(onSearch)
        dAdd.setOnClickListener(onAdd)

        val categoryAdapter = ArrayAdapter<String>(context, R.layout.simple_list_item, getDataBase().readCategory())
        sCategory.adapter = categoryAdapter

    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_online
    }

    val onBack = View.OnClickListener {
        getMainActivity().goToFragmentList()
    }

    val onSearch = View.OnClickListener {
        val query = editTextQuery.text.toString()
        if (query.contains("autor:")) {
            getBooksByAuthor(query.replace("autor:", ""))
        } else {
            getBooksByName(query)
        }
    }

    val onAdd = View.OnClickListener {
        if (editTextQuery.text.isEmpty() || sResult.count==0 ) {
            showWarningMessage("Field for query is empty!")
        }
        else {
            val book = sResult.selectedItem as Book
            book.setCategory(sCategory.selectedItem.toString())

            book.setId(book.getIdFirst())
            if(!checkTheSameBooks(book))
                getDataBase().addBook(book)
            editTextQuery.setText("")
        }
    }

    fun checkTheSameBooks(book : Book) : Boolean {

        for(b : String in getDataBase().getAllBooksFromBase())
        if(b.equals(book.getId())) return true

        return false
    }

    fun getBooksByName(query : String) {
        HttpService.getBooks("intitle:" + query, "5", "newest", booksByNameCallback)
    }

    /**
     * Callback from api when we receive books by name.
     */

    val booksByNameCallback = object : Callback<BooksResponse> {
        override fun onResponse(call: Call<BooksResponse>?, response: Response<BooksResponse>?) {
            val booksAdapter = OnlineBooksAdapter(response?.body()?.items)
            sResult.adapter = booksAdapter
        }

        override fun onFailure(call: Call<BooksResponse>?, t: Throwable?) {

        }

    }

    fun getBooksByAuthor(query : String) {
        HttpService.getBooks("inauthor:" + query, "5", "newest", booksByAuthorCallback)
    }

    val booksByAuthorCallback = object : Callback<BooksResponse> {
        override fun onResponse(call: Call<BooksResponse>?, response: Response<BooksResponse>?) {
            val booksAdapter = OnlineBooksAdapter(response?.body()?.items)
            sResult.adapter = booksAdapter
        }

        override fun onFailure(call: Call<BooksResponse>?, t: Throwable?) {

        }

    }

    fun showWarningMessage(message : String) {
        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle("Warning!")
        alertDialog.setMessage(message)
        alertDialog.setPositiveButton("OK"
        ) { dialog, which -> Toast.makeText(activity.applicationContext, "You clicked on OK", Toast.LENGTH_SHORT).show() }
        alertDialog.show()
    }

}