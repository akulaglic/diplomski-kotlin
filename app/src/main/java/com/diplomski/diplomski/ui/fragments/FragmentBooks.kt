package com.diplomski.diplomski.ui.fragments

import android.view.View
import com.diplomski.diplomski.R
import com.diplomski.diplomski.ui.adapters.BooksAdapter
import com.diplomski.diplomski.ui.fragments.base.FragmentBase
import kotlinx.android.synthetic.main.fragment_books.*

/**
 * Created by Azra on 28.3.2019.
 */
class FragmentBooks : FragmentBase() {

    override fun setupFragment(view: View?) {

        if (arguments.containsKey("category")) {
            setupBooksByCategory()
        } else if (arguments.containsKey("author")) {
            setupBooksByAuthor()
        }

        dBack.setOnClickListener(onBack())
    }

    fun onBack() = View.OnClickListener {
        getMainActivity().goToFragmentList()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_books
    }

    fun setupBooksByAuthor() {
        val author = (arguments["author"] as String)
        /*
            Since author names are stored as 'Author Name (2)' where 2 is number of authors books,
            we need to extract only his name without the number to get his id.
         */
        val authorId = getMainActivity().dataBase.getAuthorId(author.substring(0, author.length-4))
        val books = if (authorId != null) getMainActivity().dataBase.getBooksByAuthor(authorId)
                    else ArrayList()
        booksList.adapter = BooksAdapter(books, context)
    }

    fun setupBooksByCategory() {
        val category = arguments["category"] as String
        val categoryId = getMainActivity().dataBase.getCategoryId(category)
        val books = getMainActivity().dataBase.getBookByCategory(categoryId)
        booksList.adapter = BooksAdapter(books, context)
    }

}