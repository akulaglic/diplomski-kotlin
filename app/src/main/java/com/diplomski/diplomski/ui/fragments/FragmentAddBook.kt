package com.diplomski.diplomski.ui.fragments
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Toast
import com.diplomski.diplomski.R
import com.diplomski.diplomski.data.models.Book
import com.diplomski.diplomski.ui.fragments.base.FragmentBase
import kotlinx.android.synthetic.main.fragment_add_book.*
import java.io.ByteArrayOutputStream
import java.io.IOException
import java.util.*

/**
 * Created by Azra on 28.3.2019.
 */
class FragmentAddBook : FragmentBase() {
    private val PICK_IMAGE_REQUEST = 1
    private var byteArray : ByteArray? = null

    override fun setupFragment(view: View?) {
        view?.findViewById<Button>(R.id.btnCancel)?.setOnClickListener(onCancel)

        val categoryAdapter = ArrayAdapter<String>(context, R.layout.simple_list_item, getDataBase().readCategory())
        sCategoryBook.adapter = categoryAdapter
        dFindImage.setOnClickListener(onFindImage)
        dAddBook.setOnClickListener(onAddBook)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        try {
            //Set image from gallery to ImageView.

            var bitmapImage = getBitmapFromUri(data?.getData())
            val stream = ByteArrayOutputStream()
            bitmapImage.compress(Bitmap.CompressFormat.PNG, 100, stream)
            byteArray= stream.toByteArray()
            coverImage.setImageBitmap(bitmapImage)
        } catch (e: IOException) {
            e.message
        }
    }


    @Throws(IOException::class)
    private fun getBitmapFromUri(uri: Uri?): Bitmap {
        val parcelFileDescriptor = activity.contentResolver.openFileDescriptor(uri!!, "r")
        val fileDescriptor = parcelFileDescriptor!!.fileDescriptor
        val image = BitmapFactory.decodeFileDescriptor(fileDescriptor)
        parcelFileDescriptor.close()
        return image
    }

    /**
     * Go to gallery.
     */
    val onFindImage = View.OnClickListener {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        if (intent.resolveActivity(activity.packageManager) != null) {
            startActivityForResult(intent, PICK_IMAGE_REQUEST)
        }
    }

    val onCancel = View.OnClickListener {
        getMainActivity().goToFragmentList()
    }

    val onAddBook = View.OnClickListener {
        if(sCategoryBook.count == 0) {
           showWarningMessage("Category list is empty!")

        }
        if (title.text.isEmpty() || authorName.text.isEmpty()) {
            showWarningMessage("Fill all fields!")
        }
        else {
            val book = Book()
            book.setBookTitle(title.text.toString())
            book.setAuthorsName(authorName.text.toString())
            book.addAuthor(authorName.text.toString())
            book.setCategory(sCategoryBook.selectedItem.toString())
            book.setId(generateString())
            book.imageBitmap = byteArray
            getDataBase().addBook(book)
            title.setText("")
            authorName.setText("")
        }
    }

    /**
     * Generate string for book id from database.
     */

    fun generateString(): String {
        val source = "ABCDEFGHIJKLMNOPQRSTUVWXYZ123456789"
        val text = CharArray(5)
        for (i in 0 until 5) {
            text[i] = source[Random().nextInt(source.length)]
        }
        return String(text)
    }

    override fun getLayoutId(): Int {
        return  R.layout.fragment_add_book
    }

    fun showWarningMessage(message : String) {
        val alertDialog = AlertDialog.Builder(context)
        alertDialog.setTitle("Warning!")
        alertDialog.setMessage(message)
        alertDialog.setPositiveButton("OK"
        ) { dialog, which -> Toast.makeText(activity.applicationContext, "You clicked on OK", Toast.LENGTH_SHORT).show() }
        alertDialog.show()
    }

}