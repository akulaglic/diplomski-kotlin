package com.diplomski.diplomski.ui.fragments.base

import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.diplomski.diplomski.data.database.DataBaseOpenHelper
import com.diplomski.diplomski.ui.activities.ActivityMain

/**
 * Created by Azra on 28.3.2019.
 */
abstract class FragmentBase : Fragment() {

    /**
     * Returns a view for fragment to show. Using [getLayoutId] we inflate view into fragment.
     */

    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view : View = inflater!!.inflate(getLayoutId(), container,false)
        return view
    }

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupFragment(view)
    }

    /**
     * Override method to return layout resource id that we will inflate in our fragment.
     */
    abstract fun getLayoutId() : Int

    /**
     * Method contains logic of [onViewCreated] method.
     */
    abstract fun setupFragment(view: View?)

    fun getMainActivity() : ActivityMain {
        return (context as ActivityMain)
    }

    fun getDataBase() : DataBaseOpenHelper {
        return getMainActivity().dataBase
    }
}