package com.diplomski.diplomski.ui.activities

import android.os.Bundle
import android.support.v4.app.Fragment
import android.util.Log
import com.diplomski.diplomski.R
import com.diplomski.diplomski.data.database.DataBaseOpenHelper
import com.diplomski.diplomski.ui.activities.base.ActivityBase
import com.diplomski.diplomski.ui.fragments.*

/**
 * Created by Azra on 28.3.2019.
 */
class ActivityMain : ActivityBase() {

    lateinit var dataBase : DataBaseOpenHelper

    override fun getLayoutId(): Int {
        return R.layout.activity_main
    }

    override fun setupActivity() {
        dataBase = DataBaseOpenHelper(this)
        goToFragmentList()
    }

    private fun changeFragment(fragment: Fragment) {
        supportFragmentManager.beginTransaction()
                .replace(R.id.frameLayout, fragment)
                .addToBackStack(fragment.javaClass.name)
                .commitAllowingStateLoss()
    }

    fun goToFragmentList() {
        changeFragment(FragmentList())
    }

    fun goToFragmentBooks(arguments : Bundle) {
        val fragmentBooks = FragmentBooks()
        fragmentBooks.arguments = arguments
        changeFragment(fragmentBooks)
    }

    fun goToFragmentSuggest(arguments : Bundle) {
        val fragmentSuggest = FragmentSuggest()
        fragmentSuggest.arguments = arguments
        changeFragment(fragmentSuggest)
    }

    fun goToFragmentOnline() {
        changeFragment(FragmentOnline())
    }

    fun goToFragmentAddBook() {
        changeFragment(FragmentAddBook())
    }

    /**
     * If there is a fragment in back stack then remove it.
     * We do this so that when we click on back button we don't leave this activity right away, but
     * we leave it when there are no fragments left in back stack.
     */
    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount != 0) {
            supportFragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}