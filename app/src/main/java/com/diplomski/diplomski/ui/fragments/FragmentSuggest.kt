package com.diplomski.diplomski.ui.fragments

import android.Manifest
import android.annotation.SuppressLint
import android.content.ActivityNotFoundException
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.ContactsContract
import android.util.Log
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import com.diplomski.diplomski.R
import com.diplomski.diplomski.data.models.Book
import com.diplomski.diplomski.ui.activities.ActivityMain
import com.diplomski.diplomski.ui.fragments.base.FragmentBase
import kotlinx.android.synthetic.main.fragment_suggest.*
import java.util.*

/**
 * Created by Azra on 28.3.2019.
 */
class FragmentSuggest : FragmentBase () {
    var spinnerList = ArrayList<String>()
    @SuppressLint("SetTextI18n")
    override fun setupFragment(view: View?) {
        val book = arguments["book"] as Book
       description1.text = "Category: " + book.getCategory() + "\n" + "Book title: " + book.getBookTitle() + "\n" +
               "Author(s): " + book.getAuthorName() + "\n" + "Release date: " + book.getReleaseDate() +
               "\n" + "Number of pages: " + book.getNumberOfPages()
        description2.text = "Opis: " + book.getDescription()

        spinnerList = getNameEmailDetails()

        dBack.setOnClickListener(onBack)
        dSend.setOnClickListener(onSend(book))

    }
    fun onSend (book : Book) : View.OnClickListener {
        return View.OnClickListener {
            sendEmail(book)
        }
    }

    val onBack = View.OnClickListener {
        (context as ActivityMain).onBackPressed()
    }

    fun sendEmail(book : Book) {
        val index = if (sContacts.selectedItemPosition == -1) 0 else sContacts.selectedItemPosition;

        Log.i("Send email", "")
        var message = ""
        message = if(book.getAuthorNames() != null) {
            "Hello " + spinnerList[index] + ", " + "\nRead a book " +
                    book.getBookTitle() + " from " + book.getAuthors()!![0].mName + "!"
        }

        else "Hello " + spinnerList[index] +", " + "\nRead a book " +
                book.getBookTitle() + " from " + book.getAuthors()!![0].mName + "!"
        Log.i("Send email", "")
        val TO = spinnerList[index]

        val mailto = "mailto:" + TO + "?cc=" +  "&subject=" + Uri.encode("Recommendation for book") +
        "&body=" + Uri.encode(message)

        val emailIntent = Intent(Intent.ACTION_SENDTO)
        emailIntent.data = Uri.parse(mailto)

        try {
            startActivity(emailIntent)
        } catch (e : ActivityNotFoundException ) {
            Toast.makeText(activity, "There is no email client installed.",
                    Toast.LENGTH_SHORT).show()
        }
    }

    private fun getNameEmailDetails(): ArrayList<String> {
        val list = ArrayList<String>()
        if (activity.applicationContext.checkSelfPermission(Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED)
            return ArrayList()
        val names = ArrayList<String>()
        val cr = activity.contentResolver
        val cur = cr.query(ContactsContract.Contacts.CONTENT_URI, null, null, null, null)
        if (cur!!.count > 0) {
            while (cur.moveToNext()) {
                val id = cur.getString(cur.getColumnIndex(ContactsContract.Contacts._ID))
                val cur1 = cr.query(
                        ContactsContract.CommonDataKinds.Email.CONTENT_URI, null,
                        ContactsContract.CommonDataKinds.Email.CONTACT_ID + " = ?",
                        arrayOf(id), null)
                while (cur1!!.moveToNext()) {
                    //to get the contact names
                    val name = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME))
                    Log.e("Name :", name)
                    val email = cur1.getString(cur1.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA))

                    Log.e("Email", email)
                    if (email != null) {
                        names.add(name)
                        list.add(email)
                    }
                }
                cur1.close()
            }
        }

        val adp1 = ArrayAdapter<String>(context, R.layout.simple_list_item,  list)
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        sContacts.adapter = adp1
        return names
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_suggest
    }

}