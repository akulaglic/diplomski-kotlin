package com.diplomski.diplomski.ui.adapters

import android.annotation.SuppressLint
import android.app.PendingIntent.getActivity
import android.content.Context
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.View.OnClickListener
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.diplomski.diplomski.R
import com.diplomski.diplomski.data.models.Author
import com.diplomski.diplomski.data.models.Book
import com.diplomski.diplomski.ui.activities.ActivityMain
import com.diplomski.diplomski.ui.fragments.FragmentSuggest
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.element_book.view.*
import kotlinx.android.synthetic.main.fragment_online.*
import java.net.URL

/**
 * Created by Azra on 23.5.2019.
 */
class BooksAdapter (val books : ArrayList<Book>? ,
                    val context : Context) : BaseAdapter() {

    @SuppressLint("ViewHolder")
    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val book = books?.get(p0)
        val view = LayoutInflater.from(p2?.context).inflate(
                R.layout.element_book, null) as View
        view.bookName.text = book?.getBookTitle()
        view.authorName.text = book?.getAuthorName()
        view.eReleaseDate.text = book?.getReleaseDate()
        view.eNumberOfPages.text = book?.getNumberOfPages().toString()
        view.eDescription.text = book?.getDescription()
        if (book?.getImageURL() != null) {
            Picasso.get().load(book.getImageURL().toString()).into(view.eCover)
        } else if (book?.imageBitmap != null){
            view.eCover.setImageBitmap(BitmapFactory.decodeByteArray(book.imageBitmap, 0, book.imageBitmap!!.size))
        }
        view.dSuggestion.setOnClickListener(onSuggestionClickListener(book!!))

        return view
    }

    fun onSuggestionClickListener(book : Book) : OnClickListener {
        return OnClickListener {
            val bundle = Bundle()
            bundle.putSerializable("book", book)
            (context as ActivityMain).goToFragmentSuggest(bundle)
        }
    }

    override fun getItem(p0: Int): Any {
        return books?.get(p0)!!
    }

    override fun getItemId(p0: Int): Long {
        return p0.toLong()
    }

    override fun getCount(): Int {
        return books?.size!!
    }

}