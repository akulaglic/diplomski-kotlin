package com.diplomski.diplomski.ui.fragments

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.diplomski.diplomski.R
import com.diplomski.diplomski.data.models.Author
import com.diplomski.diplomski.ui.fragments.base.FragmentBase
import kotlinx.android.synthetic.main.fragment_list.*

/**
 * Created by Azra on 28.3.2019.
 */
class FragmentList : FragmentBase() {

    var whichButtonPress : String = "categories"

    override fun setupFragment(view: View?) {
        setupViews()
    }

    override fun getLayoutId(): Int {
        return R.layout.fragment_list
    }

    fun setupViews() {
        btnAddBook.setOnClickListener(onAddBook)
        btnOnline.setOnClickListener(onOnline)
        btnAddCategory.setOnClickListener(onAddCategory)
        btnSearch.setOnClickListener(onSearch)
        btnCategory.setOnClickListener(onCategories)
        btnAuthors.setOnClickListener(onAuthors)

        listOfCategories.setOnItemClickListener(onItem)
        refreshCategoriesList(getDataBase().readCategory())
        btnAddCategory.isEnabled = false
    }

    val onItem = AdapterView.OnItemClickListener { adapterView: AdapterView<*>, view1: View, position: Int, l: Long ->
        val bundle = Bundle()
        if (whichButtonPress.equals("categories")) {
            bundle.putString("category", adapterView.getItemAtPosition(position).toString())
            getMainActivity().goToFragmentBooks(bundle)
        }
        if (whichButtonPress.equals("authors")) {
            bundle.putString("author", adapterView.getItemAtPosition(position).toString())
            getMainActivity().goToFragmentBooks(bundle)
        }

    }

    val onAddBook = View.OnClickListener {
        getMainActivity().goToFragmentAddBook()
    }

    val onOnline = View.OnClickListener {
        getMainActivity().goToFragmentOnline()
    }

    val onAddCategory = View.OnClickListener {
        val query = edtSearch.text.toString()
        getDataBase().addCategory(query)
        refreshCategoriesList(getDataBase().readCategory())
        edtSearch.text.clear()
        btnAddCategory.isEnabled = false
    }

    val onSearch = View.OnClickListener {
        val query = edtSearch.text.toString()
        val adapter = listOfCategories.adapter as ArrayAdapter<*>
        adapter.filter.filter(query)
        adapter.notifyDataSetChanged()
        btnAddCategory.isEnabled = adapter.isEmpty
    }

    val onCategories = View.OnClickListener {
        whichButtonPress = "categories"
        btnAddCategory.visibility = View.VISIBLE
        btnSearch.visibility = View.VISIBLE
        edtSearch.visibility = View.VISIBLE

        refreshCategoriesList(getDataBase().readCategory())
    }

    val onAuthors = View.OnClickListener {
        whichButtonPress = "authors"
        btnAddCategory.visibility = View.GONE
        btnSearch.visibility = View.GONE
        edtSearch.visibility = View.GONE

        refreshCategoriesList(makeListOfAuthorNames())
    }

    fun makeListOfAuthorNames() : ArrayList<String>  {

        val listOfNames = ArrayList<String>()

        for (author : Author in getDataBase().authorsFromBase()) {
            listOfNames.add(author.mName + " (" + author.mNumberOfBooks.toString() + ")")
        }
        return listOfNames
    }

    fun refreshCategoriesList(listFromBase : ArrayList<String>) {
        listOfCategories.adapter = ArrayAdapter<String>(
                context,
                R.layout.categories_list_element,
                R.id.text,
                listFromBase)
    }

}